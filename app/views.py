# coding=utf-8
__author__ = 'nlfox'
from Queue import Queue
import copy
import threading
import urllib
import urllib2
import datetime
from flask import render_template, flash, redirect, session, url_for, request, g
from app import app, db
from models import User, Token, File, Feedback
from hashlib import md5
import json
import time
from functools import wraps


def token_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        inputToken = request.args.get('token', '')
        token = Token.query.filter_by(token=inputToken).first()
        if not token:
            return error('token error')
        return f(token.user, *args, **kwargs)

    return decorated_function


def error(message='sth wrong'):
    errorMsg = {
        'code': 0,
        'message': message
    }
    return json.dumps(errorMsg, ensure_ascii=False)


def info(message='ok'):
    infoMsg = {
        'code': 1,
        'message': message
    }
    return json.dumps(infoMsg, ensure_ascii=False)


@app.route('/test', methods=['GET'])
@token_required
def test(user):
    return 'OK'


######################################
# 增加note


@app.route('/note/get/<id>', methods=['GET'])
@token_required
def getNote(user, id):
    try:
        file = File.query.filter_by(user_id=user.id).filter_by(id=id).first()
    except:
        return error("404 id not found")
    return info(file.content)


@app.route('/note/edit/<id>', methods=['POST'])
@token_required
def editNote(user, id):
    fileContent = request.form['content']
    fileCache = request.form['cache']

    try:
        file = File.query.filter_by(user_id=user.id).filter_by(id=id).first()
    except:
        return error('404 not found')
    file.content = fileContent
    file.cache = fileCache
    file.update_time = datetime.datetime.now()
    file.save()
    return info('modify file ok')


@app.route('/note/edit/<id>/del', methods=['GET'])
@token_required
def delNote(user, id):
    try:
        file = File.query.filter_by(user_id=user.id).filter_by(id=id).first()
        file.delete()
    except:
        return error('404 not found')
    return info('del file ok')


@app.route('/note/get', methods=['GET'])
@token_required
def getAllNote(user):
    try:
        files = File.query.filter_by(user_id=user.id).all()
    except:
        return error('no file')
    fileList = []
    for i in files:
        fileDic = {}
        fileDic['name'] = i.name
        fileDic['fileId'] = i.id
        fileDic['updateTime'] = str(i.update_time)
        fileDic['cache'] = i.cache
        fileDic['type'] = i.type
        fileList.append(fileDic)
    return json.dumps(fileList, ensure_ascii=False)


@app.route('/note/edit/<id>/append', methods=['POST'])
@token_required
def appendNote(user, id):
    appendContent = request.form['content']
    cache = request.form['cache']
    try:
        file = File.query.filter_by(user_id=user.id).filter_by(id=id).first()
    except:
        return error('404 not found')
    fileContent = file.content
    file.cache = cache
    jsonArr = json.loads(fileContent)
    jsonArr.extend(json.loads(appendContent))
    file.content = json.dumps(jsonArr)
    file.update_time = datetime.datetime.now()
    file.save()
    return info('append ok')


########################################
# 增加

@app.route('/note/type/<type>/add', methods=['POST'])
@token_required
def addNote(user, type):
    fileContent = request.form['content']
    cache = request.form['cache']
    name = request.form['name']
    newFile = File(content=fileContent, name=name, cache=cache, type=type, user_id=user.id,
                   update_time=datetime.datetime.now())
    newFile.save()
    fileDic = {}
    fileDic["fileid"] = newFile.id
    return info(json.dumps(fileDic))


@app.route('/note/type/<type>/get', methods=['GET'])
@token_required
def showNoteList(user, type):
    # type
    files = File.query.filter_by(user_id=user.id).filter_by(type=type).all()
    fileList = []
    for i in files:
        fileDic = {}
        fileDic['name'] = i.name
        fileDic['fileId'] = i.id
        fileDic['updateTime'] = str(i.update_time)
        fileDic['cache'] = i.cache
        fileList.append(fileDic)
    return json.dumps(fileList, ensure_ascii=False)


########################################
# 用户


@app.route('/user/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        psdmd5 = md5(request.form['password'])
        password = psdmd5.hexdigest()
        u = User(username=request.form['username'],
                 email=request.form['email'], password=password, school='dlut')
        try:
            db.session.add(u)
            db.session.commit()
            return info('successfully signed up')
        except:
            return error('something goes wrong')
    return error('not POST')


@app.route('/user/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        psdmd5 = md5(request.form['password'])
        password = psdmd5.hexdigest()
        user = User.query.filter_by(
            username=username, password=password
        ).first()
        if user:
            tokenText = md5(password + str(time.time())).hexdigest()
            token = Token(user_id=user.id, token=tokenText)
            token.save()
            return tokenText
        else:
            return error('login error')
    return error('not POST')


@app.route('/feedback', methods=['POST'])
@token_required
def addFeedback(user):
    content = request.form['content']
    try:
        fb = Feedback(content=content,user_id=user.id,update_time=datetime.datetime.now())
    except:
        return error('error')
    fb.save()
    return info('feedback add ok')

@app.route('/feedback', methods=['GET'])
@token_required
def getAllFeedback(user):
    try:
        files = Feedback.query.all()
    except:
        return error('no file')
    fileList = []
    for i in files:
        fileDic = {}
        fileDic['id'] = i.id
        fileDic['content'] = i.content
        fileDic['updateTime'] = str(i.update_time)
        fileList.append(fileDic)
    return json.dumps(fileList, ensure_ascii=False)
