__author__ = 'nlfox'
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
# from flask_cors import CORS
app = Flask(__name__)
# cors = CORS(app)

app.config.from_object('config')

db = SQLAlchemy(app)

from app import views, models
#import views, models