__author__ = 'nlfox'
from app import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    username = db.Column(db.String(60), index=True, unique=True)

    email = db.Column(db.String(120), index=True, unique=True)

    password = db.Column(db.String(40), index=True)

    school = db.Column(db.String(60))

    file = db.relationship("File")

    def is_authenticated(self):
        return True

    def get_id(self):
        return unicode(self.id)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def __repr__(self):
        return '<User %r>' % self.username


class Token(db.Model):
    token = db.Column(db.String(120), index=True, primary_key=True, unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User")

    def __repr__(self):
        return '<Token %r>' % self.token

    def save(self):
        db.session.add(self)
        db.session.commit()

class File(db.Model):
    id = db.Column(db.Integer, index=True, primary_key=True,autoincrement=True)
    type = db.Column(db.Integer)
    name = db.Column(db.Text)
    content = db.Column(db.Text)
    cache = db.Column(db.Text)
    update_time = db.Column(db.TIMESTAMP)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<File %r>' % self.token

    def delete(self):
        db.session.delete(self)
        db.session.commit()
    def save(self):
        db.session.add(self)
        db.session.commit()

class Feedback(db.Model):
    id = db.Column(db.Integer, index=True, primary_key=True,autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    content = db.Column(db.Text)
    update_time = db.Column(db.TIMESTAMP)

    def __repr__(self):
        return '<Feedback %r>' % self.token

    def delete(self):
        db.session.delete(self)
        db.session.commit()
    def save(self):
        db.session.add(self)
        db.session.commit()

